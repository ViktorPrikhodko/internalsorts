﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class Sort
    {
        public void CreateElement(DataGridView dgv, Plitka P, int i)
        {
            P._Width = Convert.ToInt32(dgv.Rows[i].Cells[0].Value);
            P._Height = Convert.ToInt32(dgv.Rows[i].Cells[1].Value);
            P._Price = Convert.ToInt32(dgv.Rows[i].Cells[2].Value);
            P._Count = Convert.ToInt32(dgv.Rows[i].Cells[3].Value);
        }

        public void CreateArray(DataGridView dgv, Plitka[] PR)
        {
            for (int i = 0; i < PR.Length; i++)
            {
                PR[i] = new Plitka();
                CreateElement(dgv, PR[i], i);
            }
        }
        public void InternalSort(Plitka[] PR)
        {
            for (int i = 0; i < PR.Length - 1; i++)
            {
                int MinIndex = i;
                for (int j = i + 1; j < PR.Length; j++)
                {
                    if (PR[j]._Price < PR[MinIndex]._Price)
                    {
                        MinIndex = j;
                    }
                }
                Plitka El = PR[MinIndex];
                PR[MinIndex] = PR[i];
                PR[i] = El;
            }
        }
        public void CheckArray(DataGridView dgv, Plitka[] PR, int square)
        {
            int j = 0;
            for (int i = 0; i < PR.Length; i++)
            {
                if (PR[i]._Width * PR[i]._Height * PR[i]._Count >= square)
                {
                    dgv.Rows[j].Cells[0].Value = Convert.ToString(PR[i]._Width);
                    dgv.Rows[j].Cells[1].Value = Convert.ToString(PR[i]._Height);
                    dgv.Rows[j].Cells[2].Value = Convert.ToString(PR[i]._Price);
                    dgv.Rows[j].Cells[3].Value = Convert.ToString(PR[i]._Count);
                    j++;
                }
            }
        }
    }
}
